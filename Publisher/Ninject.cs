﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using Shared;

namespace Publisher
{
	class NinjectMain : NinjectModule
	{
		public override void Load()
		{
			Bind<IPublisher>().To<Publisher>();
			
			Bind<IRepository>().To<Repository>();

			Bind<IConfig>().ToMethod(_ => Config.GetConfig());

			Bind<IQueueConfig>().ToMethod(_ => Config.GetConfig().QueueConfig);
		}
	}
}
