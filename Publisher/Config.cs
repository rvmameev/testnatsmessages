﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace Publisher
{
	class Config : IConfig
	{
		public IQueueConfig QueueConfig { get; set; }

		public int MessageInterval { get; set; }

		public static IConfig GetConfig()
		{
			return new Config
			{
				MessageInterval = 1000,
				QueueConfig = new QueueConfig
				{ 
					Url = "localhost:4222",
					TimeoutReconnect = 3000,
					MaxCountReconnect = 3 
				}
			};
		}
	}
}
