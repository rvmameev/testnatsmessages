﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ninject;
using Shared;

namespace Publisher
{
	class Program
	{
		static void Main(string[] args)
		{
			StandardKernel kernel = new StandardKernel();

			kernel.Load<NinjectMain>();

			IConfig config = kernel.Get<IConfig>();

			IPublisher publisher = kernel.Get<IPublisher>();

			var state = new object();

			var timerCallback = new TimerCallback(obj =>
			{
				publisher.SendMessage(0);
			});

			var timer = new Timer(timerCallback, state, 0, config.MessageInterval);

			Console.ReadKey();
		}
	}
}
