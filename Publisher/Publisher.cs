﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NATS.Client;
using NLog;
using Shared;

namespace Publisher
{
	class Publisher : IPublisher
	{
		private static readonly Logger logger = LogManager.GetCurrentClassLogger();
		private readonly object sendLocker = new object();
		private readonly IQueueConfig config;
		private readonly IRepository repository;

		public Publisher(IQueueConfig config, IRepository repository)
		{
			this.config = config;
			this.repository = repository;
		}

		/// <summary>
		/// Отправляет сообщение с заданным номером. При messageId == 0 отправляет новое сообщение с новым номером.
		/// </summary>
		/// <param name="messageId">Номер сообщения</param>
		/// <returns></returns>
		public bool SendMessage(int messageId)
		{
			ConnectionFactory connectionFactory = new ConnectionFactory();

			var maxCountReconnect = config.MaxCountReconnect;

			lock (sendLocker)
			{

				while (true)
				{
					try
					{
						using (IConnection connection = connectionFactory.CreateConnection(config.Url))
						{
							var message = messageId == 0 ? repository.NewMessage() : repository.GetMessage(messageId);

							if (message == null)
								throw new Exception("message not found");

							message.TimeSend = DateTimeOffset.Now;

							connection.Publish($"{nameof(Message)}.{messageId}", message.ToBytes());

							repository.AddMessage(message);

							logger.Trace($"Сообщение {message.Id} отправлено");
						}

						break;
					}
					catch (Exception e)
					{
						logger.Error($"Ошибка отправки сообщения. {e.Message}");

						maxCountReconnect--;

						if (maxCountReconnect == 0)
							break;

						logger.Trace($"Повторная попытка через {config.TimeoutReconnect / 1000} секунд");

						Thread.Sleep(config.TimeoutReconnect);
					}
				}

			}
			if (maxCountReconnect == 0)
			{
				logger.Trace("Исчерпано количество попыток отправки сообщения");

				return false;
			}

			return true;
		}
	}
}
