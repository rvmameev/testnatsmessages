﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace Subscriber
{
	interface IConfig
	{
		IQueueConfig QueueConfig { get; }
	}

	interface ISubscriber
	{
		void Subscribe();
		void Unsubscribe();
	}
}
