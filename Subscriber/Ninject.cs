﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;
using Shared;

namespace Subscriber
{
	class NinjectMain : NinjectModule
	{
		public override void Load()
		{
			Bind<IQueueConfig>().ToMethod(_ => Config.GetConfig().QueueConfig);

			Bind<ISubscriber>().To<Subscriber>();

			Bind<IRepository>().To<Repository>();
		}
	}
}
