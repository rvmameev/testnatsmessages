﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NATS.Client;
using NLog;
using Shared;

namespace Subscriber
{
	class Subscriber : ISubscriber
	{
		private readonly static Logger logger = LogManager.GetCurrentClassLogger();
		private readonly IQueueConfig queueConfig;
		private readonly IRepository repository;

		private object waitLock = new object();

		public Subscriber(IQueueConfig queueConfig, IRepository repository)
		{
			this.queueConfig = queueConfig;
			this.repository = repository;
		}

		public void Subscribe()
		{
			EventHandler<MsgHandlerEventArgs> handler = (sender, args) =>
			{
				try
				{
					Message message = (Message)args.Message.Data.ToObject();

					message.TimeReceive = DateTimeOffset.Now;

					repository.AddMessage(message);

					logger.Trace($"Сообщение {message.Id} получено");
				}
				catch (Exception e)
				{
					logger.Error(e.Message);
				}
			};

			ConnectionFactory connectionFactory = new ConnectionFactory();

			try
			{
				IConnection connection = connectionFactory.CreateConnection(queueConfig.Url);

				using (IAsyncSubscription sub = connection.SubscribeAsync(nameof(Message)+".*", handler))
				{
					logger.Trace($"Ожидание {queueConfig.Url}");

					lock (waitLock)
					{
						Monitor.Wait(waitLock);
					}
				}
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		public void Unsubscribe()
		{
			lock (waitLock)
			{
				Monitor.Pulse(waitLock);
			}
		}
	}
}
