﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared;

namespace Subscriber
{
	class Config : IConfig
	{
		public IQueueConfig QueueConfig { get; set; }

		public static IConfig GetConfig()
		{
			return new Config
			{
				QueueConfig = new QueueConfig
				{
					Url = "localhost:4222",
					TimeoutReconnect = 3000,
					MaxCountReconnect = 3
				}
			};
		}
	}
}
