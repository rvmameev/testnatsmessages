﻿using System;

namespace Shared
{
	public interface IQueueConfig
	{
		string Url { get; }

		int TimeoutReconnect { get; }

		int MaxCountReconnect { get; }
	}

	public interface IRepository
	{
		Message NewMessage();

		Message GetMessage(int messageId);

		void AddMessage(Message message);
	}

}
