﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Shared
{
	public static class SerializableExtentions
	{
		public static byte[] ToBytes(this object obj)
		{
			if (obj == null)
				return null;

			BinaryFormatter binaryFormatter = new BinaryFormatter();

			using (MemoryStream memoryStream = new MemoryStream())
			{
				binaryFormatter.Serialize(memoryStream, obj);

				return memoryStream.ToArray();
			}
		}

		public static object ToObject(this byte[] bytes)
		{
			if (bytes == null)
				return null;

			using (MemoryStream memoryStream = new MemoryStream())
			{
				BinaryFormatter binaryFormatter = new BinaryFormatter();

				memoryStream.Write(bytes, 0, bytes.Length);

				memoryStream.Seek(0, SeekOrigin.Begin);

				return binaryFormatter.Deserialize(memoryStream);
			}
		}
	}
}
