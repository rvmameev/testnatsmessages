﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NLog;
using Shared;

namespace Shared
{
	public class Repository : IRepository
	{
		private readonly static Logger logger = LogManager.GetCurrentClassLogger();
		private readonly static string dbFileName = Path.Combine(Directory.GetCurrentDirectory(), "db.data");
		private readonly static ConcurrentBag<Message> messages = new ConcurrentBag<Message>();
		private readonly static object dbLocker = new object();

		static Repository()
		{
			try
			{
				using (var stream = new FileStream(dbFileName, FileMode.OpenOrCreate, FileAccess.Read))
				using (var reader = new StreamReader(stream))
				{
					while (true)
					{
						var line = reader.ReadLine();

						if (line == null)
							break;

						messages.Add(JsonConvert.DeserializeObject<Message>(line));
					}
				}
			}
			catch (Exception e)
			{
				logger.Error(e.Message);

				throw;
			}
		}

		private static string getDbHash()
		{
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();

			using (var stream = new FileStream(dbFileName, FileMode.OpenOrCreate))
			using (var reader = new StreamReader(stream))
			{
				var bytes = Encoding.UTF8.GetBytes(reader.ReadToEnd());

				return BitConverter.ToString(md5.ComputeHash(bytes)).Replace("-", "");
			}
		}

		private static int getNewId()
		{
			return messages.Select(m => m.Id).DefaultIfEmpty(0).Max() + 1;
		}

		public Message NewMessage()
		{
			lock (dbLocker)
			{
				var newId = getNewId();

				var message =
					new Message
					{
						Id = newId,
						Text = $"Message {newId}",
						RepositoryHash = getDbHash()
					};

				return message;
			}
		}

		public Message GetMessage(int messageId)
		{
			return messages.FirstOrDefault(x => x.Id == messageId);
		}

		public void AddMessage(Message message)
		{
			lock (dbLocker)
			{
				messages.Add(message);

				using (var stream = new FileStream(dbFileName, FileMode.OpenOrCreate, FileAccess.Write))
				using (var writer = new StreamWriter(stream))
				{
					stream.Seek(0, SeekOrigin.End);

					writer.WriteLine(JsonConvert.SerializeObject(message));
				}
			}

		}
	}
}
