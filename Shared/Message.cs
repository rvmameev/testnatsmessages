﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
	[Serializable]
	public class Message
	{
		public int Id { get; set; }

		public string Text { get; set; }

		public DateTimeOffset TimeSend { get; set; }

		public DateTimeOffset TimeReceive { get; set; }

		public string RepositoryHash { get; set; }
	}
}
