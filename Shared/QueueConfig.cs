﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
	public class QueueConfig : IQueueConfig
	{
		public string Url { get; set; }

		public int TimeoutReconnect { get; set; }

		public int MaxCountReconnect { get; set; }
	}
}
